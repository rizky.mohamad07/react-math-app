/**
 * @return {number}
 */
export const sum = (a, b) => {
  return a + b;
};

/**
 * @return {number}
 */
export const multiply = (a, b) => {
  return a * b;
};

/**
 * @return {Array}
 */
export const generatePrimeNumber = (n) => {
  let primes = [];
  if (n < 1) {
    primes.push("Nothing");
  } else {
    primes.push(2);

    let i = 3;
    for (let count = 2; count <= n; i++) {
      let c;
      for (c = 2; c < i; c++) {
        if (i % c === 0) break;
      }

      if (c === i) {
        primes.push(i);
        count++;
      }
    }
  }
  return primes;
};

/**
 * @return {Array}
 */
export const generateFibbonaciSequence = (n) => {
  let sequence = [];
  let f1 = 0,
    f2 = 1,
    i;

  if (n < 1) {
    sequence.push("Nothing");
  } else {
    sequence.push(0);
    for (i = 2; i <= n; i++) {
      sequence.push(f2);
      let next = f1 + f2;
      f1 = f2;
      f2 = next;
    }
  }
  return sequence;
};
