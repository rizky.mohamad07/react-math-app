import {
  sum,
  multiply,
  generatePrimeNumber,
  generateFibbonaciSequence,
} from "utils/Math";

describe("Math Utils", () => {
  test("sum Function", function () {
    expect(sum(1, 2)).toEqual(3);
    expect(sum(2, 2)).toEqual(4);
  });

  test("multiply Function", function () {
    expect(multiply(1, 0)).toEqual(0);
    expect(multiply(2, 2)).toEqual(4);
  });

  test("generatePrimeNumber Function", function () {
    expect(generatePrimeNumber(0)).toEqual(["Nothing"]);
    expect(generatePrimeNumber(-1)).toEqual(["Nothing"]);
    expect(generatePrimeNumber(1)).toEqual([2]);
    expect(generatePrimeNumber(3)).toEqual([2, 3, 5]);
  });

  test("generateFibbonaciSequence Function", function () {
    expect(generateFibbonaciSequence(0)).toEqual(["Nothing"]);
    expect(generatePrimeNumber(-1)).toEqual(["Nothing"]);
    expect(generateFibbonaciSequence(1)).toEqual([0]);
    expect(generateFibbonaciSequence(3)).toEqual([0, 1, 1]);
  });
});
