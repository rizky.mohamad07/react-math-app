import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";

/**
 * @param {React.Node} Component
 * @param {String} route
 */
export function renderWithRouter(Component, route = "/") {
  return render(
    <Router history={createMemoryHistory(route)}>{Component}</Router>
  );
}
