import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";

import { renderWithRouter } from "utils/test";

import FibonacciSequence from "containers/FibonacciSequence";

beforeEach(cleanup);
describe("Fibonacci Sequence Container", () => {
  test("should render", function () {
    const setUp = renderWithRouter(<FibonacciSequence />);
    expect(setUp.unmount()).toBe(true);
  });

  test("Input Sequence Length", function () {
    const setUp = renderWithRouter(<FibonacciSequence />);

    fireEvent.change(setUp.getByTestId("input-length"), {
      target: { value: "3" },
    });
    expect(setUp.getByTestId("input-length").value).toBe("3");
  });

  test("Submit Fibonacci Sequence Length", function () {
    const setUp = renderWithRouter(<FibonacciSequence />);
    fireEvent.change(setUp.getByTestId("input-length"), {
      target: { value: "3" },
    });
    const submitEvent = fireEvent.click(setUp.getByTestId("submit-button"));
    expect(submitEvent).toBe(true);
  });
});
