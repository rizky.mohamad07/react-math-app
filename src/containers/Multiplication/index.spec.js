import React from "react";
import { cleanup, fireEvent } from "@testing-library/react";

import { renderWithRouter } from "utils/test";

import Multiplication from "containers/Multiplication";

beforeEach(cleanup);
describe("Fibonacci Sequence Container", () => {
  test("should render", function () {
    const setUp = renderWithRouter(<Multiplication />);
    expect(setUp.unmount()).toBe(true);
  });

  test("Input First Number", function () {
    const setUp = renderWithRouter(<Multiplication />);

    fireEvent.change(setUp.getByTestId("input-first-number"), {
      target: { value: "3" },
    });
    expect(setUp.getByTestId("input-first-number").value).toBe("3");
  });

  test("Input Second Number", function () {
    const setUp = renderWithRouter(<Multiplication />);

    fireEvent.change(setUp.getByTestId("input-second-number"), {
      target: { value: "3" },
    });
    expect(setUp.getByTestId("input-second-number").value).toBe("3");
  });

  test("Submit Numbers", function () {
    const setUp = renderWithRouter(<Multiplication />);
    fireEvent.change(setUp.getByTestId("input-first-number"), {
      target: { value: "3" },
    });
    fireEvent.change(setUp.getByTestId("input-second-number"), {
      target: { value: "3" },
    });
    const submitEvent = fireEvent.click(setUp.getByTestId("submit-button"));
    expect(submitEvent).toBe(true);
  });
});
