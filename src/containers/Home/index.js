import React from "react";
import styled from "styled-components";
import Sidebar from "components/Sidebar";
import Header from "components/Header";
import Footer from "components/Footer";

import { Layout } from "antd";
const { Content } = Layout;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`;

const StyledContent = styled(Content)`
  margin-left: 48px;
  margin-top: 48px;
`;

const StyledContentContainer = styled.div`
  padding: 24;
  min-height: 360px;
  height: 100%
  background: #fff;
`;

function Home() {
  return (
    <StyledLayout>
      <Sidebar />
      <Layout>
        <Header />
        <StyledContent>
          <StyledContentContainer>
            Welcome to Simple Math App
          </StyledContentContainer>
        </StyledContent>
        <Footer />
      </Layout>
    </StyledLayout>
  );
}

export default Home;
