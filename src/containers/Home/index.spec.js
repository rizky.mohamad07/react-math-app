import React from "react";
import { renderWithRouter } from "utils/test";

import Home from "containers/Home";

describe("Home Container", () => {
  test("should render", function () {
    const setUp = renderWithRouter(<Home />);
    expect(setUp.unmount()).toBe(true);
  });
});
