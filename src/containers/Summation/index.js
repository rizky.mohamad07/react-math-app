import React from "react";
import styled from "styled-components";
import Sidebar from "components/Sidebar";
import Header from "components/Header";
import Footer from "components/Footer";
import { sum } from "utils/Math";
import { Form, InputNumber, Button, Modal, Typography, Layout } from "antd";

const { Content } = Layout;
const { Title } = Typography;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`;

const StyledContent = styled(Content)`
  margin: 48px;
`;

const StyledContentContainer = styled.div`
  padding: 16px;
  min-height: 360px;
  height: 100%;
  width: 100%;
  background: #fff;
`;

const StyledInputNumber = styled(InputNumber)`
  width: 100%;
`;

const formLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};

const formTailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

function Summation() {
  const showResult = (result) => {
    Modal.info({
      title: "Summation Result",
      content: <div>{result}</div>,
      onOk() {},
    });
  };

  const onFinish = (values) => {
    showResult(sum(values.firstNumber, values.secondNumber));
  };

  const onFinishFailed = () => {
    Modal.error({
      title: "Error",
      content: "Please resolve the input errors first",
    });
  };
  return (
    <StyledLayout>
      <Sidebar />
      <Layout>
        <Header />
        <StyledContent>
          <StyledContentContainer>
            <Title level={2}>Summation</Title>
            <Form
              {...formLayout}
              name="basic"
              initialValues={{ firstNumber: 0, secondNumber: 0 }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <Form.Item
                label="First Number"
                name="firstNumber"
                rules={[
                  { required: true, message: "Please input your First Number" },
                ]}
              >
                <StyledInputNumber data-testid="input-first-number" />
              </Form.Item>

              <Form.Item
                label="Second Number"
                name="secondNumber"
                rules={[
                  {
                    required: true,
                    message: "Please input your Second Number",
                  },
                ]}
              >
                <StyledInputNumber data-testid="input-second-number" />
              </Form.Item>
              <Form.Item {...formTailLayout}>
                <Button
                  type="primary"
                  htmlType="submit"
                  data-testid="submit-button"
                >
                  Show Result
                </Button>
              </Form.Item>
            </Form>
          </StyledContentContainer>
        </StyledContent>
        <Footer />
      </Layout>
    </StyledLayout>
  );
}

export default Summation;
