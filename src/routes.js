import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Loader from "components/Loader";
const Home = lazy(() => import("containers/Home"));
const Summation = lazy(() => import("containers/Summation"));
const Multiplication = lazy(() => import("containers/Multiplication"));
const PrimeNumber = lazy(() => import("containers/PrimeNumber"));
const FibonacciSequence = lazy(() => import("containers/FibonacciSequence"));

const Routes = () => (
  <Router>
    <Suspense fallback={<Loader />}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/summation" component={Summation} />
        <Route path="/multiplication" component={Multiplication} />
        <Route path="/prime-number" component={PrimeNumber} />
        <Route path="/fibonacci-sequence" component={FibonacciSequence} />
      </Switch>
    </Suspense>
  </Router>
);

export default Routes;
