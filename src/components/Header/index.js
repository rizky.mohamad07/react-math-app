import * as React from "react";
import styled from "styled-components";
import { Layout, Typography } from "antd";
const { Header } = Layout;
const { Title } = Typography;
const StyledHeader = styled(Header)`
  padding: 0;
  background: #fff;
`;

const StyledHeaderContent = styled.div`
  text-align: center;
`;

function HeaderComponent() {
  return (
    <StyledHeader>
      <StyledHeaderContent>
        <Title>Simple Math App</Title>
      </StyledHeaderContent>
    </StyledHeader>
  );
}

export default HeaderComponent;
