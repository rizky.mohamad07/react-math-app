import React from "react";
import { shallow } from "enzyme";
import { Router } from "react-router-dom";
import Sidebar from "./index";

it("renders without crashing", () => {
  const historyMock = { push: jest.fn(), location: {}, listen: jest.fn() };
  shallow(
    <Router history={historyMock}>
      <Sidebar />
    </Router>
  );
});
