import * as React from "react";
import styled from "styled-components";
import { Layout, Menu } from "antd";
import { useHistory } from "react-router-dom";

const { Sider } = Layout;

const StyledLogo = styled.div`
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
  text-align: center;
`;

const StyledTitle = styled.h3`
  color: #fff;
  font-weight: bold;
`;

function Sidebar() {
  const history = useHistory();
  return (
    <Sider breakpoint="lg" collapsedWidth="0">
      <StyledLogo>
        <StyledTitle>Navigation</StyledTitle>
      </StyledLogo>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[
          history.location.pathname.split("/")[1] || "home",
        ]}
      >
        <Menu.Item key="home" onClick={() => history.push("/")}>
          <span className="nav-text">Home</span>
        </Menu.Item>
        <Menu.Item key="summation" onClick={() => history.push("/summation")}>
          <span className="nav-text">Summation</span>
        </Menu.Item>
        <Menu.Item
          key="multiplication"
          onClick={() => history.push("multiplication")}
        >
          <span className="nav-text">Multiplication</span>
        </Menu.Item>
        <Menu.Item
          key="prime-number"
          onClick={() => history.push("/prime-number")}
        >
          <span className="nav-text">Prime Number</span>
        </Menu.Item>
        <Menu.Item
          key="fibonacci-sequence"
          onClick={() => history.push("/fibonacci-sequence")}
        >
          <span className="nav-text">Fibonacci Sequence</span>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default Sidebar;
