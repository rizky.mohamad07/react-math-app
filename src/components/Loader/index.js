import * as React from "react";
import styled from "styled-components";

import { Layout, Skeleton } from "antd";
const { Sider, Content } = Layout;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`;
const StyledContent = styled(Content)`
  margin: 48px;
`;

const StyledContentContainer = styled.div`
  padding: 16px;
  min-height: 360px;
  height: 100%;
  width: 100%;
  background: #fff;
`;
function Loader() {
  return (
    <StyledLayout>
      <Sider></Sider>
      <Layout>
        <StyledContent>
          <StyledContentContainer>
            <Skeleton active />
            <Skeleton active />
          </StyledContentContainer>
        </StyledContent>
      </Layout>
    </StyledLayout>
  );
}

export default Loader;
