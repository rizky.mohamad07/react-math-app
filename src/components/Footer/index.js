import * as React from "react";
import styled from "styled-components";

import { Layout } from "antd";
const { Footer } = Layout;
const StyledFooter = styled(Footer)`
  text-align: center;
`;

function FooterComponent() {
  return (
    <StyledFooter>Simple Math App ©2020 Created by Mohamad Rizky</StyledFooter>
  );
}

export default FooterComponent;
