const {
  override,
  fixBabelImports,
  addWebpackAlias,
  addLessLoader,
} = require("customize-cra");
const path = require("path");

module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#1DA57A" },
  }),
  addWebpackAlias({
    components: path.resolve(__dirname, "./src/components"),
    containers: path.resolve(__dirname, "./src/containers"),
    utils: path.resolve(__dirname, "./src/utils"),
  })
);
